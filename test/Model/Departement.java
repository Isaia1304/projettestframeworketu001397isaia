/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import annotation.MapAnnotation;
import java.sql.Connection;
import java.util.Vector;
import modelview.ModelView;

/**
 *
 * @author itu
 */
//@ListerDept(url={"listerDepartement","insert"},obj="Departement",fonction={"lister","inserer"})
public class Departement {
    int id;
    String nom;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    @MapAnnotation(url="listes")
    public ModelView listes(){
        String [] list=new String[5];
        list[0]="Isaia";
        list[1]="Mitantsoa";
        list[2]="Dina";
        list[3]="Bal";
        list[4]="Tanjona";
        ModelView view =new ModelView();
        view.setData(list);
        view.setUrl("liste.jsp");
        return view;
    }
    
    @MapAnnotation(url="addia")
   public ModelView add(){
        ModelView mv=new ModelView();
        mv.setData(this.getNom()+"-"+this.getId());
        mv.setUrl("add.jsp");
        return mv;
    }
   
    
    
}
